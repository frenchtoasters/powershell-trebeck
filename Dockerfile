FROM mcr.microsoft.com/powershell

WORKDIR /home/trebeck/scripts
RUN useradd trebeck -d /home/trebeck

# Get the module PowerCLI modules installed in our container
# TODO: Identify the exact VMware.XXXX modules you need in script
RUN pwsh -c Install-Module -Name VMware.PowerCLI -Force

# Copy in the scripts and modules needed
COPY *.ps1 /home/trebeck/scripts
COPY *.psm1 /home/trebeck/scripts
RUN chown -R trebeck /home/trebeck/

USER trebeck
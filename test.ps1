# Tell the folks how we feel
Write-Host 'Suck it Trabeck'

# Set some configuration items so that PowerCLI doesnt yell at us
Set-PowerCLIConfiguration -Scope:User -ParticipateInCeip:$false -Confirm:$false
Set-PowerCLIConfiguration -InvalidCertificateAction:ignore -Confirm:$false

# We have to import the specific libraries that we are going to use cause not VMware.PowerCLI has modules that are
# not currently supported on PowerShell Core, who knows when they will update that...
Import-Module -Name VMware.VimAutomation.Core

# Import a custom module we loaded in the container
Import-Module ./test.psm1

# Call the module we import
Hello-Trebeck "Alex" "Texas"
function Hello-Trebeck {
    param (
        [string] $Name,
        [string] $Host
    )
    Write-Host "You cant do that $Name from $Host"
}